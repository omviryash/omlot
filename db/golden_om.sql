-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 22, 2015 at 03:19 AM
-- Server version: 5.5.42-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `avadh`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `draw_amount` double DEFAULT '0',
  `jackput_amount` double DEFAULT '0',
  `draw_amount2` double DEFAULT '0',
  `jackput_amount2` double DEFAULT '0',
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `product_price`, `draw_amount`, `jackput_amount`, `draw_amount2`, `jackput_amount2`) VALUES
(1, 'GUJARAT GOLD VAPI', '11', 100, 200, 0, 0),
(2, 'NAV RATANAM', '11', 100, 200, 0, 0),
(3, '', '11', 100, 200, 50, 100);

-- --------------------------------------------------------

--
-- Table structure for table `draw`
--

CREATE TABLE IF NOT EXISTS `draw` (
  `draw_id` int(11) NOT NULL AUTO_INCREMENT,
  `drawdatetime` datetime DEFAULT NULL,
  `win_product_id` int(11) DEFAULT NULL,
  `win_amount` double NOT NULL DEFAULT '0',
  `is_jackpot` enum('NO','YES') NOT NULL,
  `win_product_id2` int(11) DEFAULT NULL,
  `win_amount2` double NOT NULL DEFAULT '0',
  `is_jackpot2` enum('NO','YES') NOT NULL,
  `percent` double NOT NULL DEFAULT '0',
  `last_update_time` datetime DEFAULT NULL,
  `last_update_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`draw_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2810 ;

--
-- Dumping data for table `draw`
--

INSERT INTO `draw` (`draw_id`, `drawdatetime`, `win_product_id`, `win_amount`, `is_jackpot`, `win_product_id2`, `win_amount2`, `is_jackpot2`, `percent`, `last_update_time`, `last_update_ip`) VALUES
(2755, '2015-08-22 08:30:00', 10, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2756, '2015-08-22 08:45:00', 3, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 08:44:14', '1.39.13.103'),
(2757, '2015-08-22 09:00:00', 4, 200, 'YES', NULL, 0, 'NO', 0, '2015-08-22 08:59:15', '1.39.13.103'),
(2758, '2015-08-22 09:15:00', 7, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 09:14:13', '1.39.13.103'),
(2759, '2015-08-22 09:30:00', 6, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 09:29:12', '1.39.13.103'),
(2760, '2015-08-22 09:45:00', 5, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 09:44:16', '1.39.13.103'),
(2761, '2015-08-22 10:00:00', 10, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2762, '2015-08-22 10:15:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2763, '2015-08-22 10:30:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2764, '2015-08-22 10:45:00', 1, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 10:44:51', '1.39.13.103'),
(2765, '2015-08-22 11:00:00', 7, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2766, '2015-08-22 11:15:00', 7, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2767, '2015-08-22 11:30:00', 2, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 11:29:12', '1.39.13.103'),
(2768, '2015-08-22 11:45:00', 3, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 11:44:21', '1.39.13.103'),
(2769, '2015-08-22 12:00:00', 4, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 11:59:20', '1.39.13.103'),
(2770, '2015-08-22 12:15:00', 6, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 12:14:28', '1.39.13.103');
INSERT INTO `draw` (`draw_id`, `drawdatetime`, `win_product_id`, `win_amount`, `is_jackpot`, `win_product_id2`, `win_amount2`, `is_jackpot2`, `percent`, `last_update_time`, `last_update_ip`) VALUES
(2771, '2015-08-22 12:30:00', 9, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 12:29:52', '1.39.13.103'),
(2772, '2015-08-22 12:45:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2773, '2015-08-22 13:00:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2774, '2015-08-22 13:15:00', 3, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2775, '2015-08-22 13:30:00', 9, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2776, '2015-08-22 13:45:00', 8, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 13:44:24', '1.39.12.157'),
(2777, '2015-08-22 14:00:00', 1, 100, 'NO', NULL, 0, 'NO', 0, '2015-08-22 13:59:43', '1.39.12.157'),
(2778, '2015-08-22 14:15:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2779, '2015-08-22 14:30:00', 8, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2780, '2015-08-22 14:45:00', 5, 100, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2781, '2015-08-22 15:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2782, '2015-08-22 15:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2783, '2015-08-22 15:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2784, '2015-08-22 15:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2785, '2015-08-22 16:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2786, '2015-08-22 16:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2787, '2015-08-22 16:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2788, '2015-08-22 16:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2789, '2015-08-22 17:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2790, '2015-08-22 17:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2791, '2015-08-22 17:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2792, '2015-08-22 17:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2793, '2015-08-22 18:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2794, '2015-08-22 18:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2795, '2015-08-22 18:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2796, '2015-08-22 18:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2797, '2015-08-22 19:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2798, '2015-08-22 19:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2799, '2015-08-22 19:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2800, '2015-08-22 19:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2801, '2015-08-22 20:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2802, '2015-08-22 20:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2803, '2015-08-22 20:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2804, '2015-08-22 20:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2805, '2015-08-22 21:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2806, '2015-08-22 21:15:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2807, '2015-08-22 21:30:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2808, '2015-08-22 21:45:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL),
(2809, '2015-08-22 22:00:00', NULL, 0, 'NO', NULL, 0, 'NO', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `company_id`, `product_name`) VALUES
(1, 2, 'Durga Yantra'),
(2, 2, 'Ganpati Yantra'),
(3, 2, 'Gayatri Yantra'),
(4, 2, 'Hanuman Yantra'),
(5, 2, 'Kalsarp Yantra'),
(6, 2, 'Sunny Yantra'),
(7, 2, 'Shree Yantra'),
(8, 2, 'Surya Yantra'),
(9, 2, 'Vasikaran Yantra'),
(10, 2, 'Vastudosh Yantra');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE IF NOT EXISTS `receipt_details` (
  `receipt_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`receipt_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=124288 ;

--
-- Dumping data for table `receipt_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `receipt_master`
--

CREATE TABLE IF NOT EXISTS `receipt_master` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_time` datetime NOT NULL,
  `receipt_ip` varchar(15) DEFAULT NULL,
  `retailer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `draw_id` int(11) NOT NULL,
  `hash_key` varchar(13) DEFAULT NULL,
  `receipt_cancel` tinyint(4) NOT NULL DEFAULT '0',
  `cancel_time` datetime DEFAULT NULL,
  `cancel_ip` varchar(15) DEFAULT NULL,
  `receipt_scan` tinyint(4) NOT NULL DEFAULT '0',
  `user_commission` double NOT NULL DEFAULT '0',
  `win_time` datetime DEFAULT NULL,
  `win_ip` varchar(15) DEFAULT NULL,
  `scan_time` datetime DEFAULT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50008 ;

--
-- Dumping data for table `receipt_master`
--


-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) DEFAULT NULL,
  `cr_dr` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `trans_for_id` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `transaction_time` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

--
-- Dumping data for table `transaction`
--


-- --------------------------------------------------------

--
-- Table structure for table `trans_for`
--

CREATE TABLE IF NOT EXISTS `trans_for` (
  `trans_for_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_for` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_for_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `current_balance` double NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '0=admin,1=retailer',
  `user_commission` double NOT NULL DEFAULT '0',
  `user_ip` varchar(15) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `online` datetime DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10023 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `current_balance`, `usertype`, `user_commission`, `user_ip`, `is_active`, `online`, `city`, `note`) VALUES
(1, 'admin', 'cb3bae31bb1c443fbf3db8889055f2fe', 1270, 0, 0, '', 1, '2015-08-22 14:03:20', NULL, NULL),
(30, 'omtest', 'bb6aa39fe8d3c31e5cd1d6129c01acb2', 45492.85, 0, 5, NULL, 1, '2015-08-02 09:08:08', NULL, NULL),
(60, 'rajkot1', 'e10adc3949ba59abbe56e057f20f883e', 2864.4100000002236, 1, 5, NULL, 1, '2015-07-26 12:05:08', 'Rajkot', 'Test'),
(67, '2040270004', '827ccb0eea8a706c4c34a16891f84e7b', 4879.070000008412, 1, 3, NULL, 0, '2015-07-05 13:03:50', 'Rajkot', ''),
(69, '2040270006', 'bf8251c13ca5704e6253e6820bb3ca0f', 940.2800000001358, 1, 4, NULL, 1, '2015-08-22 10:45:01', 'Navsari', ''),
(70, '2040270007', 'dd5c611393044aa4244fd198722712e7', 96.75000000006493, 1, 4, NULL, 1, '2015-08-21 12:25:36', 'Navsari', ''),
(72, '2040270008', '4ea83a55e9506d4e2cd754d72f193a9e', 3497.9600000001496, 1, 4, NULL, 1, '2015-08-22 14:45:03', 'Navsari', ''),
(9999, 'superadmin', '0a4b76fd825db8c32b8fcd53101a7b05', 10000, 9, 0, NULL, 1, NULL, NULL, NULL),
(10000, '2040270009', 'a5b6bafbfff09a6861876aeb47b5c826', 7400.160000000191, 1, 4, NULL, 1, '2015-08-22 14:46:10', 'Navsari', ''),
(10003, '2040270011', 'c44a43b6d71f9c4aa9292a3f677b09d3', 1311.125999999936, 1, 4, NULL, 1, '2015-08-22 14:45:02', 'Navsari', ''),
(10005, 'rajkot2', '46d045ff5190f6ea93739da6c0aa19bc', 26533.92999999842, 1, 0, NULL, 1, '2015-08-20 00:08:35', '', ''),
(10009, 'demo', '827ccb0eea8a706c4c34a16891f84e7b', 0, 1, 0, NULL, 1, '2015-08-04 19:33:51', '', ''),
(10016, 'omtest2', 'bb6aa39fe8d3c31e5cd1d6129c01acb2', 10000, 1, 0, NULL, 1, '2015-05-16 17:50:36', 'Rajkot', ''),
(10022, '2040270005', 'd18d562e87bea884986fda9fc2788b55', 5795.999999999987, 1, 4, NULL, 1, '2015-08-22 14:45:03', '', ''),
(10018, '20407012', 'e9c5626093a8ad17d658630076e2fc86', 405.11999999999676, 1, 4, NULL, 1, '2015-08-18 12:16:18', '', ''),
(10019, '20407013', '74c01e6038a04cf3bb0c8345218d3e80', 5039.568000000094, 1, 4, NULL, 1, '2015-08-22 14:45:03', '', ''),
(10020, '20407014', 'baef5f36621b0519deec0e90f64bfcbc', 2549.7600000000893, 1, 4, NULL, 1, '2015-08-22 14:49:46', '', ''),
(10021, '20407015', '63499f36462d3648a6a3cac73ba4de07', 1136.3199999999376, 1, 4, NULL, 1, '2015-08-22 14:45:04', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE IF NOT EXISTS `users_login` (
  `users_login_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_time` datetime NOT NULL,
  `user_ip` varchar(15) DEFAULT NULL,
  `valid_invalid` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_login`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `win`
--
