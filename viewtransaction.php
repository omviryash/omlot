<?php
include_once('includes/basepath.php');
if(isset($_GET['id']) && $_GET['id'] != '')
{
	$sSQL = "SELECT * FROM transaction WHERE transaction_id='".$_GET['id']."'";
	$rs = mysql_query($sSQL);
	if(mysql_num_rows($rs) > 0){
		$row = mysql_fetch_array($rs);
		if(strtolower($row["cr_dr"]) == "credit"){
			mysql_query("UPDATE users SET current_balance = current_balance - ".$row["amount"]." WHERE user_id = ".$row["retailer_id"]);
		}else{
			mysql_query("UPDATE users SET current_balance = current_balance + ".$row["amount"]." WHERE user_id = ".$row["retailer_id"]);
		}
		mysql_query("DELETE FROM transaction WHERE transaction_id='".$_GET['id']."'");
	}
	header("Location:viewtransaction.php");
	exit;
}	
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	 <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
</head>

<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
	<div id="mainWrapper" style="margin-top:20px;">
		<div class="box-body table-responsive">
				<div style="margin-left:140px;margin-bottom:10px;"><b>Transaction</b></div>
				<table id="example1" class="table table-bordered table-striped" align="center" width="80%" cellspacing="0" cellpadding="4" border="1">
					<thead>
						<tr>
              <th width="100px">Retailer</th>
              <th width="160px">Date</th>
              <th width="200px">Note</th>
              <th width="140px">Transaction</th>
							<th width="140px">Point</th>
							<th width="100px">Action</th>
            </tr>
        	</thead>
        	<tbody>                  
					<?php
					$qry = "SELECT * FROM transaction order by transaction_time desc";
					$res = mysql_query($qry) or print(mysql_error());
					while($row = mysql_fetch_array($res))
					{
					?>
          	<tr>
              <td><?php echo rtnretailer($row['retailer_id']);?></td>
              <td align="center"><?php echo $row['transaction_time'];?></td>
							<td><?php echo $row['notes'];?>&nbsp;</td>
							<td align="center"><?php echo $row['cr_dr'];?></td>
							<td align="right"><?php echo $row['amount'];?></td>
							<td align="center"><a href="javascript:void(0)" onclick="confirmtoDelete(<?php echo $row['transaction_id'];?>)">Delete</a></td>
						</tr>      
					<?php } ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->
	</div> 
</body>
</html>
<script language="javascript">
	jQuery(document).ready(function(){
		//$("#example1").dataTable();
	})
	
	function confirmtoDelete(cid){
		var r = confirm("Confirm to delete this transaction?");
		if (r == true) {
		    location.replace("viewtransaction.php?id="+cid);
		}
	}
	</script>
