Hardik Zala on Mar 3, 2015 at 12:05 AM to vipul@omvir.com

1).

http://localhost/omlot/omlot/ajax.php
Parameter = "act=loadData"

As per the current coding, 
	a. First take the current datetime and convert it in timestamp using strtotime function and then DEDUCT 5 from the converted timestamp. (Line 29)
	
	b. then check if drawId is post with ajax request or not. If not blank, find the records from draw table as per drawID. if drawId is not post with ajax request, we will get those records whose drawdatetime is greater than current datetime with group by drawdatetime asc.
	
	c. we will get results in json_encode format. we get below parameter as per the below sequence.
		1. Difference of seconds
		2. Next draw time (This will display above the yantra image slideshow as "Time")
		3. Current datetime (This will display in dashboard on right side upper right corner.)
		4. draw id
		

The above the first ajax request when we run dashboard.php.

2). 
http://localhost/omlot/omlot/ajax.php
Parameter = "act=getresult&FullTime="

As per the current coding,
	a. First check that can we get this variable "ctime"? If we can get this "ctime" variable, then get H:i:s from the "ctime" variable. If "ctime" does not get, get the current H:i:s (hour:minute:second).
	
	b. then after we get those records from draw table whose drawdatetime is equal to current date and also whose drawdatetime is less than or equal to H:i:s (as per above point "a"). and then store in array.
	
	c. then after, we get those records from draw table whose drawdatetime is equal to current date and also whole drawdatetime is greater than H:i:s (as per above point "a"). then store in array.
	
	d. then encode array in json format.
	
Suggestion. :: Here, we are looping two time whereas difference of select query is only in time condition.
so we can use "between" condition in the time. so that we can write only time select query. and also we can check time in loop and get the result as per our requirement.

3).
dashboard.php 

- Every 10 seconds, one ajax call will fire to check if the network is available or not. If available, smiley face icon will display on right side upper corner. If not available, angry smiley face icon will display on right side upper corner.


4).
http://localhost/omlot/omlot/ajax.php
Parameter = "act=loadData"

- when draw is completed, one ajax request will fire named "loadData". This will load the next draw.

5).
http://localhost/omlot/omlot/ajax.php
act=getdrawresult&draw_id=6

- After the "act=loadData" ajax request completed, another ajax request will run named "getdrawresult".
- According to the coding in "getdrawresult" ajax request, check if the drawId is passed or not. If drawId is passed, then store drawId in one variable. If drawId is not passed, then drawId will be zero.

- Then after find those records from draw table whose drawdatetime is equal to current datetime and drawId is equal to as per find above drawId.

- Store results in array and encode array in json format.
