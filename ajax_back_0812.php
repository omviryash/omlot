<?php
include_once('includes/basepath.php');

if(isset($_POST["act"]) && trim($_POST["act"]) == "loadData"){
	$arrayVal = array();
	if(isset($_POST["FullDate"]) && trim($_POST["FullDate"]) != ""){
		$newdate = date("Y-m-d H:i:s",strtotime(trim($_POST["FullDate"])));
	}else{
		$newdate = date("Y-m-d H:i:s");
	}
	$sSQL = "SELECT draw_id,drawdatetime FROM draw WHERE drawdatetime > '".$newdate."' ORDER BY drawdatetime LIMIT 0,1";
	
	$rs = mysql_query($sSQL);
	if(mysql_num_rows($rs) > 0){
		$row = mysql_fetch_array($rs);
		$arrayVal['draw_id'] = $row['draw_id'];
		$arrayVal['currentFullDateTime'] = date("m/d/Y H:i:s",strtotime($row['drawdatetime']));
		$arrayVal['currentTime'] = date("h:i A",strtotime($row['drawdatetime']));
	}
	
	echo json_encode($arrayVal);
}

/* if(isset($_POST["act"]) && trim($_POST["act"]) == "setWinProduct"){
	include_once('perdraw.php');
}
 */

if(isset($_POST["act"]) && trim($_POST["act"]) == "upcoming"){
	$arrayVal = array();
	$currentMinute = intval(date('i'));
	$currentHour = date('G');
	if($currentMinute >= 0 && $currentMinute < 15) $compareTime = date("H:")."15:00";
	if($currentMinute >= 15 && $currentMinute < 30) $compareTime = date("H:")."30:00";
	if($currentMinute >= 30 && $currentMinute < 45) $compareTime = date("H:")."45:00";
	if($currentMinute >= 45){
		$newHR = ($currentHour+1);
		if($newHR < 10)
			$compareTime = "0".$newHR.":00:00";
		else
			$compareTime = $newHR.":00:00";
	}
	//$sSQL = "SELECT * FROM draw WHERE drawdatetime >= '".date("Y-m-d H:i:s")."' AND drawdatetime < '".$compareTime."' ORDER BY drawdatetime LIMIT 0,1";
	//$sSQL = "SELECT draw_id,DATE_FORMAT(drawdatetime,'%m/%d/%Y %H:%i:%s') as currentFullDateTime,DATE_FORMAT(drawdatetime,'%h:%i %p') as currentTime FROM draw WHERE DATE_FORMAT(drawdatetime,'%H:%i:%s') >= '".$compareTime."' AND DATE_FORMAT(drawdatetime,'%Y-%m-%d') = '".date("Y-m-d")."' ORDER BY drawdatetime";
	$sSQL = "SELECT draw_id,DATE_FORMAT(drawdatetime,'%m/%d/%Y %H:%i:%s') AS currentFullDateTime,DATE_FORMAT(drawdatetime,'%h:%i %p') AS currentTime "
    . " FROM draw "
    . " WHERE DATE_FORMAT(drawdatetime,'%H:%i:%s') >= '".$compareTime."' "
    . "   AND DATE_FORMAT(drawdatetime,'%Y-%m-%d') = '".date("Y-m-d")."' "
    . " ORDER BY drawdatetime";
	$rs = mysql_query($sSQL);//today's only pending record we need
	$rows = array();
	while($row = mysql_fetch_assoc($rs)){
		$rows[] = $row;
	}
	$sSQL = "SELECT draw_id,DATE_FORMAT(drawdatetime,'%m/%d/%Y %H:%i:%s') as currentFullDateTime,DATE_FORMAT(drawdatetime,'%h:%i %p') as currentTime FROM draw WHERE DATE_FORMAT(drawdatetime,'%Y-%m-%d') > '".date("Y-m-d")."' ORDER BY drawdatetime";
	$rs = mysql_query($sSQL);
	while($row = mysql_fetch_assoc($rs)){
		$rows[] = $row;
	}
	
	echo json_encode($rows);
}

if(isset($_POST["act"]) && trim($_POST["act"]) == "getresult"){
	$arrayVal = array();
	if(isset($_POST["ctime"]) && trim($_POST["ctime"]) != ""){
		$newdate = date("H:i:s",strtotime(trim($_POST["ctime"])));
	}else{
		$newdate = date("H:i:s");
	}
	
	$sSQL = "SELECT win_product_id,DATE_FORMAT(d.drawdatetime,'%h:%i %p') as currentTime,is_jackpot FROM draw d,product p WHERE DATE_FORMAT(d.drawdatetime,'%Y-%m-%d') = '".date('Y-m-d')."' AND DATE_FORMAT(drawdatetime,'%H:%i:%s') <= '".$newdate."' AND p.product_id = d.win_product_id ORDER BY d.drawdatetime";
	$rs = mysql_query($sSQL);
	$rows = array();
	$totalRow = mysql_num_rows($rs);
	$i = 0; $j = 1;
	while($row = mysql_fetch_assoc($rs)){
		//$rows[] = $row;
		$rows[$i]['win_product_id'] = $row["win_product_id"];
		$rows[$i]['currentTime'] = $row["currentTime"];
		$rows[$i]['is_jackpot'] = $row["is_jackpot"];
		//$rows[$i]['is_last'] = 0;
		if($j == $totalRow)
			$rows[$i]['is_last'] = 1;
		else
			$rows[$i]['is_last'] = 0;
		$i++;
		$j++;
	}
	$sSQL = "SELECT win_product_id,DATE_FORMAT(d.drawdatetime,'%h:%i %p') as currentTime,is_jackpot FROM draw d WHERE DATE_FORMAT(d.drawdatetime,'%Y-%m-%d') = '".date('Y-m-d')."' AND DATE_FORMAT(drawdatetime,'%H:%i:%s') > '".$newdate."' ORDER BY d.drawdatetime";
	$rs = mysql_query($sSQL);
	while($row = mysql_fetch_assoc($rs)){
		//$rows[] = $row;
		$rows[$i]['win_product_id'] = "0";
		$rows[$i]['currentTime'] = $row["currentTime"];
		$rows[$i]['is_jackpot'] = 'NO';
		$rows[$i]['is_last'] = 0;
		$i++;
	}
		
	echo json_encode($rows);
}

if(isset($_POST["act"]) && trim($_POST["act"]) == "getwinner" && isset($_POST["hash_key"]) && trim($_POST["hash_key"]) != ""){
	$msg = "";
	$sSQL = "SELECT draw_id,receipt_id,receipt_scan,receipt_cancel,company_id,retailer_id FROM receipt_master WHERE hash_key = '".trim($_POST["hash_key"])."'";
	$rs = mysql_query($sSQL);
	if(mysql_num_rows($rs) > 0){
		$row = mysql_fetch_array($rs);
		if($row["receipt_cancel"] == 1){
			$msg = "cancel";
		}elseif($row["receipt_scan"] == 1){
			$msg = "scan";
		}elseif($row["retailer_id"] != $_SESSION['user_id']){
			$msg = "noretailer";
		}else{
			
			$isWinner = 0;
			$sSQL = "SELECT win_product_id,drawdatetime,win_amount FROM draw WHERE draw_id = ".$row["draw_id"];
			$rs1 = mysql_query($sSQL) or print(mysql_error());
			if(mysql_num_rows($rs1) > 0){
				$row1 = mysql_fetch_array($rs1);
				if($row1["drawdatetime"] <= date("Y-m-d H:i:s")){
					if(!is_null($row1["win_product_id"])){
						
						$sSQL = "SELECT quantity FROM receipt_details WHERE product_id = ".$row1["win_product_id"]." AND receipt_id = ".$row["receipt_id"];
						$rs3 = mysql_query($sSQL) or print(mysql_error());
						if(mysql_num_rows($rs3) > 0){
							$row3 = mysql_fetch_array($rs3);
							mysql_query("UPDATE users SET current_balance = current_balance + ".($row3["quantity"]*$row1["win_amount"])." WHERE user_id = ".$_SESSION['user_id']);
							$isWinner = 1;
						}
						mysql_query("UPDATE receipt_master SET receipt_scan = 1,scan_time='".date('Y-m-d H:i:s')."' WHERE receipt_id = ".$row["receipt_id"]);
						
					}
					if($isWinner == 1){
						$msg = 'winner'; 
						mysql_query("UPDATE receipt_master SET win_time='".date('Y-m-d H:i:s')."',win_ip='".$_SERVER['REMOTE_ADDR']."' WHERE receipt_id = ".$row["receipt_id"]);
					}else{
						$msg = 'nowinner';
					}
				}else{
					$msg = 'pending';
				}
			}else{
				$msg = 'nodraw';
			}
		}
	}else{
		$msg = 'nofound';
	}
	echo $msg;
}

if(isset($_POST["act"]) && trim($_POST["act"]) == "getBalance"){
	if(isset($_SESSION['user_id'])){
		$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
		$rs = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($rs) > 0){
			$row = mysql_fetch_array($rs);
			echo formatAmt($row["current_balance"]);
		}
	}
}

if(isset($_POST["act"]) && trim($_POST["act"]) == "canReceipt"){
	$isAllowCancel = 0;
	//$sSQL = "SELECT draw_id,receipt_id,user_commission FROM receipt_master WHERE company_id = ".$company_id." AND drawdatetime > '".date("Y-m-d H:i:s")."' AND receipt_time < '".date("Y-m-d H:i:s")."' ORDER BY receipt_id DESC LIMIT 0,1";
	$sSQL = "SELECT rm.receipt_id,rm.user_commission FROM receipt_master rm,draw d WHERE rm.company_id = ".$company_id." AND d.drawdatetime > '".date("Y-m-d H:i:s")."' AND rm.draw_id = d.draw_id AND rm.retailer_id = ".$_SESSION['user_id']." AND rm.receipt_cancel = 0 ORDER BY rm.receipt_id DESC LIMIT 0,1";
	$rs = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($rs) > 0){
		$row = mysql_fetch_array($rs);
				
				$amt = 0;
				$sSQL = "SELECT quantity,product_price FROM receipt_details WHERE receipt_id = ".$row["receipt_id"];
				$rs2 = mysql_query($sSQL) or print(mysql_error());
				if(mysql_num_rows($rs2) > 0){
					while($row2 = mysql_fetch_array($rs2)){
						$amt = $amt + ($row2["quantity"]*$row2["product_price"]);
						mysql_query("UPDATE users SET current_balance = current_balance + ".($row2["quantity"]*$row2["product_price"])." WHERE user_id = ".$_SESSION['user_id']);
					}	
				}
				
				if($amt > 0 && $row["user_commission"] > 0){
					$newamt = ($amt * $row["user_commission"])/100;
					mysql_query("UPDATE users SET current_balance = current_balance - ".$newamt." WHERE user_id = ".$_SESSION['user_id']);
				}
				
				$isAllowCancel = 1;
		mysql_query("UPDATE receipt_master SET receipt_cancel = 1,cancel_time='".date('Y-m-d H:i:s')."',cancel_ip='".$_SERVER['REMOTE_ADDR']."' WHERE receipt_id = ".$row["receipt_id"]);
	}
	if($isAllowCancel == 1) echo '1'; else echo '0';
}
?>